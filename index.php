<?
    header('Content-type: text/html;charset="utf-8"');
    header('Access-Control-Allow-Origin: *');
    define('AMO_DOMAIN', 'amocrm.ru');
    define('AMO_PROTOCOL', 'https');
    define('AUTO_BUILD', TRUE);
    define('CHECK_LIB_VERSION', TRUE);
    $start = microtime(TRUE);
    file_put_contents(__DIR__.'\write.txt',$_POST['id'].', ', FILE_APPEND);
    $id_lead = explode(",", $_POST['id']);
    $query = making_query($id_lead,'leads');
    Auth();
    $result = query($query);
    $arr = [];
    foreach ($result['_embedded']['items'] as $key => $value) {
        $arr[] = [
            'name' => $value['name'],
            'create_at' => $value['created_at'],
            'tags' => $value['tags'],
            'custom_fields' => $value['custom_fields'],
            'contacts' => $value['contacts'],
            'company' => $value['company']
        ];
    }
//        $arr[$key]['name'] = $value['name'];
//        $arr[$key]['create_at'] = $value['created_at'];
//        $arr[$key]['tags'] = $value['tags'];
//        $arr[$key]['custom_fields'] = $value['custom_fields'];
//        $arr[$key]['contacts'] = $value['contacts'];
//        $arr[$key]['company'] = $value['company'];
    // ВОЗМОЖНО КОГДА-НИБУДЬ Я БУДУ ТЕБЯ ПИЛИТЬ
    // 95% ЧТО НЕТ, НО ЧУДО БЫВАЕТ
    // ЗАПИСЬ ИМЕН КОНТАКТОВ И КОМПАНИЙ В МАССИВ СДЕЛКИ
//    foreach ($arr as $value) {
            // КОНТАКТЫ
    //       $query = making_query($value['contacts']['id'],'contacts');
    //        var_dump($query);
    //       //       $result = query($query);
    ////       var_dump( $result);
    //       $result = get_name($result);
    ////       var_dump( $result);
    //       $value['contacts'] = $result;
    ////       var_dump( $value['contacts']);
    //       // КОМПАНИИ
    ////    $query = making_query($value['company'],'company');
    ////    $result = query($result);
    ////    $result = get_name($result);
    ////    $value['company'] = $result;
//    }
    function get_name($arr) {
        $array_name_entity = [];
            foreach ($arr['_embedded']['items'] as $key => $value){
                $array_name_entity[] = $value['name'];
            }
        return $array_name_entity;
    }
    function making_query ($arr,$type) {
        $query = "api/v2/$type?";
            foreach ($arr as $value) {
                $query = $query."id%5B%5D=$value&";
                $query = rtrim($query);
            }
        return $query;
    }
    function query($query){
            $link = "https://ngri.amocrm.ru/$query";
            //echo $link;
            $headers[] = "Content-Type: application/json";
            //Curl options
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($curl, CURLOPT_USERAGENT, "amoCRM-API-client/2.0");
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($curl, CURLOPT_URL, $link);
            curl_setopt($curl, CURLOPT_HEADER, FALSE);
            curl_setopt($curl, CURLOPT_COOKIEFILE, dirname(__FILE__) . "/cookie.txt");
            curl_setopt($curl, CURLOPT_COOKIEJAR, dirname(__FILE__) . "/cookie.txt");
            $out = curl_exec($curl);
    //        $code=curl_getinfo($curl,CURLINFO_HTTP_CODE); // МОЖЕТ ПРИГОДИТСЯ ДЛЯ ОТЛОВА НЕЧИСТИ
            curl_close($curl);
            $result = json_decode($out, TRUE);
            return $result;
    }
    function Auth()
    {
        $user = [
            'USER_LOGIN' => 'ngrigoriev@team.amocrm.com',
            'USER_HASH' => 'a5000a74c7f25ea31a811f7f1170f5f2784ec1c6'
        ];
        $subdomain = 'ngri';
        $link = 'https://' . $subdomain . '.amocrm.ru/private/api/auth.php?type=json';
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
        curl_setopt($curl, CURLOPT_URL, $link);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($user));
        curl_setopt($curl, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
        curl_setopt($curl, CURLOPT_HEADER, FALSE);
        curl_setopt($curl, CURLOPT_COOKIEFILE, dirname(__FILE__) . '/cookie.txt');
        curl_setopt($curl, CURLOPT_COOKIEJAR, dirname(__FILE__) . '/cookie.txt');
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        $out = curl_exec($curl);
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        $code = (int)$code;
        $errors = [
            301 => 'Moved permanently',
            400 => 'Bad request',
            401 => 'Unauthorized',
            403 => 'Forbidden',
            404 => 'Not found',
            500 => 'Internal server error',
            502 => 'Bad gateway',
            503 => 'Service unavailable'
        ];
        try {
            if ($code != 200 && $code != 204) {
                    if (isset($errors[$code])) {
                        $error = $errors[$code];
                    } else {
                        $error = 'Undescribed error';
                    }
                throw new Exception($error, $code);
            }
        } catch (Exception $E) {
            die('Ошибка: ' . $E->getMessage() . PHP_EOL . 'Код ошибки: ' . $E->getCode());
        }
        $Response = json_decode($out, TRUE);
        $Response = $Response['response'];
        return $Response;
    }
